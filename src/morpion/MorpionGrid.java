package morpion;

public class MorpionGrid {
	
	private int[][] grid;
	
	public MorpionGrid() {
		grid = new int[3][3];
	}

	public int[][] getGrid() {
		return grid;
	}

}
