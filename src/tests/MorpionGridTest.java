package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import morpion.MorpionGrid;

public class MorpionGridTest {
	
	MorpionGrid morpionGrid = new MorpionGrid();
	
	@Test
	public void testGrid() {
		
		assertEquals(3, morpionGrid.getGrid().length );
		for (int[] row : morpionGrid.getGrid()) {
			assertEquals(3, row.length);
			for (int col : row) {
				assertEquals(0, col);
			}
		}
		
	}

}
